#!/usr/local/bin/elvish

use ./scripts/lib/config

var scripts-dir = ./scripts

put $scripts-dir/*.elv | each {|s|
  echo (styled "Executing "$s green)
  (external $s)
}

if (or (not-eq [(git status --porcelain $config:vars-dir)] []) ^
       (not-eq [(git status --porcelain $config:files-dir)] [])) {
  echo (styled "Comitting the following changes:" green)
  git diff $config:vars-dir $config:files-dir
  git add $config:vars-dir $config:files-dir
  git commit -m "Automated update of config files"
  git push
}
