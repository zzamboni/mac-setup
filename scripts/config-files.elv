#!/usr/local/bin/elvish
# This script is written in Elvish. If you don't have it, get it from https://elv.sh/get/
# Give Elvish a try! It's an awesome shell.

use ./lib/config
use str
use path

# Structure is: [ src dest copy-path ]
# If copy-path is present and $true, then the directory structure starting at
# $HOME is recreated under dest.
# For example [ ~/foo/bar $config:files-dir/boo $true ]
# will result in $config:files-dir/boo/foo/bar being created.
var files-dirs = [
  [ ~/.config/topgrade.toml                  $config:files-dir/homefiles/ $true ]
  [ ~/.config/starship.toml                  $config:files-dir/homefiles/ $true ]
  [ ~/.gitconfig                             $config:files-dir/homefiles/ $true ]
  [ ~/.gitattributes                         $config:files-dir/homefiles/ $true ]
  [ ~/.config/bat/                           $config:files-dir/bat/ ]
  [ ~/Library/LaunchAgents/environment.plist $config:files-dir/homefiles/ $true ]
  [ ~/.config/proselint/                     $config:files-dir/homefiles/ $true ]
  [ ~/Library/"Application Support"/Code/User/settings.json $config:files-dir/homefiles/ $true ]
  [ ~/.config/ghostty/                       $config:files-dir/homefiles/ $true ]
]

each {|pair|
  var src = $pair[0]
  var dest = $pair[1]
  if (and (> (count $pair) 2) $pair[2]) {
    var base-path = (str:replace ~/ "" $src)
    if (not (path:is-dir $src)) {
      set base-path = (dirname $base-path)
    }
    set dest = $pair[1]/$base-path
    mkdir -p $dest
  }
  if (path:is-dir $src) {
    rsync -a --delete $src $dest
  } else {
    if (path:is-regular $src) {
      cp $src $dest
    } else {
      echo Skipping $src which does not exist.
    }
  }
} $files-dirs
