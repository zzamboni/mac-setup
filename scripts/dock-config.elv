#!/usr/local/bin/elvish
# This script is written in Elvish. If you don't have it, get it from https://elv.sh/get/
# Give Elvish a try! It's an awesome shell.

use ./lib/config
use re
use str

var @dock-items = (dockutil --list | each {|l| put [(str:split "\t" $l)] })

var count = (num 1)
config:write dockitems_to_persist [
  (each {|item|
      if (eq $item[2] 'persistent-apps') {
        put [
          &name= $item[0]
          &path= (re:replace '^file://' '' (re:replace '%20' ' ' $item[1]))
          &pos=  $count
        ]
        set count = (+ $count 1)
      }
  } $dock-items)
]
