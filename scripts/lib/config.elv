# Elvish library with common configuration and code for the
# configuration-setting script

# If you don't have Elvish, get it from https://elv.sh/get/
# Give Elvish a try! It's an awesome shell.

# Directory where variable files will be stored
var vars-dir = host_vars/127.0.0.1

# Directory where other config files are stored
var files-dir = files

# Read JSON through the pipe and prettify it on stdout.  Purely
# cosmetic! /usr/bin/json_pp is installed by default on macOS, replace
# with something else if you want, or with /bin/cat if you don't want
# to prettify at all.
fn json-prettify {
  /usr/bin/json_pp -json_opt canonical,pretty
}

# Write a data structure to JSON file.
# $key is the name of the Ansible variable. The resulting file will be
# a JSON dictionary containing an element named 'key' and having as
# value the contents of $data.
# By default the filename will be $key.json for consistency, but you
# can pass a different filename (without the .json) extension in the
# &file option.
fn write {|key data &file=''|
  if (==s $file '') { set file = $key }
  put [&$key= $data] | to-json | json-prettify > $vars-dir/$file.json
}

# Print a progress/step message. Blue by default, override color with
# the &color option.
fn msg {|msg &color=blue|
  echo (styled $msg $color)
}
