#!/usr/local/bin/elvish
# This script is written in Elvish. If you don't have it, get it from https://elv.sh/get/
# Give Elvish a try! It's an awesome shell.

use str
use ./lib/config

# Return the dependencies of a package
fn package-dep-tree {|p tree|
  put $p
  if (has-key $tree $p) {
    each {|d|
      set d = (str:trim-suffix $d '.ARCH')
      if (not-eq $d $p) {
        package-dep-tree $d $tree
      }
    } $tree[$p][depends]
  }
}

config:msg "Reading TeX Live catalog..."

# We read the full Tex Live catalog as a JSON list (produced by tlmgr)
# and store it in memory indexed by package name for easier reference
# below.
var pkg-tree = [&]
each {|e|
  set pkg-tree[$e[name]] = $e
} (tlmgr info --list --json | from-json)

config:msg "Building list of default TinyTeX packages..."

# The idea is to get a list of ALL the packages that get installed as
# part of the TinyTeX base install, so that we can later extract only
# the packages that have been additionally installed by the user.

# We start with the list of packages explicitly installed by the
# TinyTeX install scripts. This list comes from:
# - The install-base script: https://github.com/yihui/tinytex/blob/master/tools/install-base.sh#L55
# - The pkgs-custom list:    https://github.com/yihui/tinytex/blob/master/tools/install-unx.sh#L20
var starting-pkg-list = [latex-bin luatex xetex (curl -s -L https://yihui.org/gh/tinytex/tools/pkgs-custom.txt)]

# From this list, we add the packages that get installed as dependencies.
var default-packages = [&]
each {|p|
  package-dep-tree $p $pkg-tree | each {|t| set default-packages[$t] = $true }
} $starting-pkg-list

config:msg "Generating list of TinyTeX packages to install..." &color=green

# From the catalog, we write out the packages that are installed and
# not in $default-packages. We also skip the architecture-specific
# packages (as those are always brought in as dependencies of their
# main package).
config:write tinytex_packages [
  (keys $pkg-tree | each {|i|
      if (and $pkg-tree[$i][installed] ^
              (not (str:contains $i ".x86")) ^
              (not (has-key $default-packages $i))) {
        echo $i
      }
  } | sort)
]
