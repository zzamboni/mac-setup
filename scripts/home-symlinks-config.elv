#!/usr/local/bin/elvish
# This script is written in Elvish. If you don't have it, get it from https://elv.sh/get/
# Give Elvish a try! It's an awesome shell.

use ./lib/config

# First get all the symlinks in the home dir
config:write home_symlinks [
  (ls ~ | each {|f|
      var src = ''
      if ?(set src = (/usr/bin/readlink ~/$f)) {
        put [&src=$src &dst=$f]
      }
})]
