#!/usr/local/bin/elvish
# This script is written in Elvish. If you don't have it, get it from https://elv.sh/get/
# Give Elvish a try! It's an awesome shell.

# This script creates the following file:
# Brewfile, with the list of installed taps, formulas, casks and MAS apps

use ./lib/config
use str

config:msg "Generating Brewfile..."
brew bundle --file=$config:files-dir/Brewfile --force --no-lock --all dump
