help:
	echo "make all - run everything"
	echo "make touchid - fix touchid sudo setup"

all:
	ansible-playbook playbook.yml -i inventory -K

touchid:
	ansible-playbook playbook.yml -i inventory -K -t touchid
